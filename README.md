# Backuper

![License](https://img.shields.io/badge/license-MIT-blue.svg) [![Latest Stable Version](https://img.shields.io/packagist/v/demlen/backuper.svg)](https://packagist.org/packages/demlen/backuper)

A [Flarum](http://flarum.org) extension. Backuper

### Installation

Use [Bazaar](https://discuss.flarum.org/d/5151-flagrow-bazaar-the-extension-marketplace) or install manually with composer:

```sh
composer require demlen/backuper
```

### Updating

```sh
composer update demlen/backuper
php flarum cache:clear
```

### Links

- [Packagist](https://packagist.org/packages/demlen/backuper)
