import SettingsModal from 'flarum/components/SettingsModal';

export default class YandexSettingsModal extends SettingsModal {
  className() {
    return 'YandexSettingsModal Modal--small';
  }

  title() {
    return app.translator.trans('flarum-auth-yandex.admin.yandex_settings.title');
  }

  form() {
    return [
      <div className="Form-group">
        <label>{app.translator.trans('flarum-auth-yandex.admin.yandex_settings.app_id_label')}</label>
        <input className="FormControl" bidi={this.setting('flarum-auth-yandex.app_id')}/>
      </div>,

      <div className="Form-group">
        <label>{app.translator.trans('flarum-auth-yandex.admin.yandex_settings.app_secret_label')}</label>
        <input className="FormControl" bidi={this.setting('flarum-auth-yandex.app_secret')}/>
      </div>
    ];
  }
}
