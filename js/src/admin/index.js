import app from 'flarum/app';

import YandexSettingsModal from './components/YandexSettingsModal';

app.initializers.add('flarum-auth-yandex', () => {
  app.extensionSettings['flarum-auth-yandex'] = () => app.modal.show(new YandexSettingsModal());
});
