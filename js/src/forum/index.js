import { extend } from 'flarum/extend';
import app from 'flarum/app';
import LogInButtons from 'flarum/components/LogInButtons';
import LogInButton from 'flarum/components/LogInButton';

app.initializers.add('flarum-auth-yandex', () => {
  extend(LogInButtons.prototype, 'items', function(items) {
    items.add('yandex',
      <LogInButton
        className="Button LogInButton--yandex"
        icon="fab fa-yandex"
        path="/auth/yandex">
        {app.translator.trans('flarum-auth-yandex.forum.log_in.with_yandex_button')}
      </LogInButton>
    );
  });
});
